package com.example.musicplayer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import com.example.musicplayer.MusicService.MusicBinder;
import com.facebook.FacebookSdk;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.ShareOpenGraphAction;
import com.facebook.share.model.ShareOpenGraphContent;
import com.facebook.share.model.ShareOpenGraphObject;
import com.facebook.share.widget.ShareDialog;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import android.app.ProgressDialog;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.app.Activity;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.MediaController.MediaPlayerControl;
import android.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.widget.Toast;

import io.fabric.sdk.android.Fabric;
import parserRss.RssReader;

/*

 * 
 *   2015
 */

public class MainActivity extends ActionBarActivity implements MediaPlayerControl {

	//song list variables
    public String URL="http://feeds.feedburner.com/TEDTalks_audio";
    private ArrayList<Song> songList;
	private ListView songView;

	//service
	private MusicService musicSrv;
	private Intent playIntent;
	//binding
	private boolean musicBound=false;

	//controller
	private MusicController controller;

	//activity and playback pause flags
	private boolean paused=false, playbackPaused=false;
    public final Handler mHandler = new Handler();
    ProgressDialog dialog;
    private ServiceConnection musicConnection;
    Toolbar toolbar;

    ImageButton end;
    ImageButton share;

    public static String consumerKey = "MTlHTwn2UAZ3V2IJaAgdp48rV";
    public static String consumerSecret = "NZtSjhNsCBKFh065sIO0OMIA89GPkcigk7ufUk2a1HeWyqSqeQ";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

        //social init
        FacebookSdk.sdkInitialize(getApplicationContext());
        //tw init
        TwitterAuthConfig authConfig =
                new TwitterAuthConfig(consumerKey,
                        consumerSecret);
        Fabric.with(this, new Twitter(authConfig));
        Fabric.with(this, new TweetComposer()); // to post twitter

        //tool bar
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        end= (ImageButton)findViewById(R.id.tool_end);
        share = (ImageButton)findViewById(R.id.tool_share);
        registerForContextMenu(share);
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("open", "ok");
                openContextMenu(v);
            }
        });
        end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopService(playIntent);
                musicSrv=null;
                System.exit(0);

            }
        });
        share.setEnabled(false);



        //retrieve list view
		songView = (ListView)findViewById(R.id.song_list);
		//instantiate list
		/*
        Song song = new Song("http://download.ted.com/talks/TrevorAaronson_2015U.mp3?apikey=172BB350-0207","Ted audio","Cat");
        songList.add(song);

        Song song2 = new Song(
                "http://download.ted.com/talks/TonyFadell_2015.mp3?apikey=172BB350-0207","In this funny, breezy talk","00:16:41");
        songList.add(song2);*/
		//create and set adapter
        songList = new ArrayList<>();
        try {
            songList = new getMp3Link().execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        SongAdapter songAdt = new SongAdapter(this, songList);
        songView.setAdapter(songAdt);

        /*songView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                songPick(position);
            }
        });*/


		//setup controller
		setController();

        //connect to the service
        //private ServiceConnection
        musicConnection = new ServiceConnection(){

            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                MusicBinder binder = (MusicBinder)service;
                //get service
                musicSrv = binder.getService();
                //pass list
                musicSrv.setList(songList);
                musicBound = true;
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                musicBound = false;
            }
        };
	}





    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        super.onCreateContextMenu(menu, v, menuInfo);

        menu.setHeaderTitle("Share your favorite article");
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.camer_menu, menu);
    }


    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.share_facebook:
                //Toast.makeText(context, "Selected Take Photo", Toast.LENGTH_SHORT).show();
                shareFacebook();
                break;

            case R.id.share_twitter:
                //Toast.makeText(context, "Selected Gallery", Toast.LENGTH_SHORT).show();
               shareTwitter();

                break;

            case R.id.share_email:
                shareEmail();
                break;
            default:
                return super.onContextItemSelected(item);
        }
        return true;
    }

    public void shareFacebook(){

        ShareDialog shareDialog = new ShareDialog(MainActivity.this);


        ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse(getSongisPlaying()))
                .build();


        shareDialog.show(content);
    }
    public void shareTwitter()
    {
        TweetComposer.Builder builder = new TweetComposer.Builder(this)
                .text(getSongisPlaying());


        builder.show();

    }
    public void shareEmail()
    {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{" "});
        i.putExtra(Intent.EXTRA_SUBJECT, "my favorite song");
        i.putExtra(Intent.EXTRA_TEXT   , getSongisPlaying());
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(MainActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }

    }




    public void updateProgressBar() {
        mHandler.postDelayed(mUpdateTimeTask, 100);
    }

    /**
     * Background Runnable thread
     * */
    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {


            // Running this thread after 100 milliseconds
            if(dialog.isShowing() && isPlaying())
            {
                dialog.dismiss();
                mHandler.removeCallbacks(mUpdateTimeTask);
                controller.show(0);
                controller.setFocusable(true);
                controller.setSelected(true);
            }

            mHandler.postDelayed(this, 100);
        }
    };

	//start and bind the service when the activity starts
	@Override
	protected void onStart() {
		super.onStart();
		if(playIntent==null){
			playIntent = new Intent(this, MusicService.class);
			bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE);
			startService(playIntent);
		}
	}

	//user song select
	public void songPicked(View view){
        if(controller.isShowing())
            controller.hide();
		musicSrv.setSong(Integer.parseInt(view.getTag().toString()));
		musicSrv.playSong();
        share.setEnabled(true);
        //dialog = ProgressDialog.show(MainActivity.this,"Loading","");
        dialog = ProgressDialog.show(MainActivity.this,"","Loading...");
        updateProgressBar();
		if(playbackPaused){
			setController();
			playbackPaused=false;
		}
		//controller.show(0);
	}

    public void songPick(int i){
        if(controller.isShowing())
            controller.hide();
        musicSrv.setSong(i);
        musicSrv.playSong();
        share.setEnabled(true);
        //dialog = ProgressDialog.show(MainActivity.this,"Loading","");
        dialog = ProgressDialog.show(MainActivity.this,"","Loading...");
        updateProgressBar();
        if(playbackPaused){
            setController();
            playbackPaused=false;
        }
        //controller.show(0);
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		//menu item selected
		switch (item.getItemId()) {
		case R.id.action_shuffle:
			musicSrv.setShuffle();
			break;
		case R.id.action_end:
			stopService(playIntent);
			musicSrv=null;
			System.exit(0);
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	//method to retrieve song info from device
	public void getSongList(){
		//query external audio
		ContentResolver musicResolver = getContentResolver();
		Uri musicUri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
		Cursor musicCursor = musicResolver.query(musicUri, null, null, null, null);
		//iterate over results if valid
		if(musicCursor!=null && musicCursor.moveToFirst()){
			//get columns
			int titleColumn = musicCursor.getColumnIndex
					(android.provider.MediaStore.Audio.Media.TITLE);
			int idColumn = musicCursor.getColumnIndex
					(android.provider.MediaStore.Audio.Media._ID);
			int artistColumn = musicCursor.getColumnIndex
					(android.provider.MediaStore.Audio.Media.ARTIST);
			//add songs to list
			do {
				long thisId = musicCursor.getLong(idColumn);
				String thisTitle = musicCursor.getString(titleColumn);
				String thisArtist = musicCursor.getString(artistColumn);
				//songList.add(new Song(thisId, thisTitle, thisArtist));
			} 
			while (musicCursor.moveToNext());
		}
	}
    public String getSongisPlaying()
    {
        return musicSrv.getSongisPlaying();
    }

	@Override
	public boolean canPause() {
		return true;
	}

	@Override
	public boolean canSeekBackward() {
		return true;
	}

	@Override
	public boolean canSeekForward() {
		return true;
	}

	@Override
	public int getAudioSessionId() {
		return 0;
	}

	@Override
	public int getBufferPercentage() {
		return 0;
	}

	@Override
	public int getCurrentPosition() {
		if(musicSrv!=null && musicBound && musicSrv.isPng())
			return musicSrv.getPosn();
		else return 0;
	}

	@Override
	public int getDuration() {
		if(musicSrv!=null && musicBound && musicSrv.isPng())
			return musicSrv.getDur();
		else return 0;
	}

	@Override
	public boolean isPlaying() {
		if(musicSrv!=null && musicBound)
			return musicSrv.isPng();
		return false;
	}

	@Override
	public void pause() {
		playbackPaused=true;
		musicSrv.pausePlayer();
	}

	@Override
	public void seekTo(int pos) {
		musicSrv.seek(pos);
	}

	@Override
	public void start() {
		musicSrv.go();
	}

	//set the controller up
	private void setController(){
		controller = new MusicController(this);
		//set previous and next button listeners
		controller.setPrevNextListeners(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				playNext();
			}
		}, new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				playPrev();
			}
		});
		//set and show

		controller.setAnchorView(findViewById(R.id.song_list));
        controller.setMediaPlayer(this);
		controller.setEnabled(true);
        controller.setFocusable(true);
        //controller.setSelected(true);


        //controller.show();
	}

	private void playNext(){
		musicSrv.playNext();
        //dialog = ProgressDialog.show(MainActivity.this,"Loading","");
        dialog = ProgressDialog.show(MainActivity.this,"","Loading...");
        updateProgressBar();
		if(playbackPaused){ 
			setController();
			playbackPaused=false;
		}
		//controller.show(0);


	}

	private void playPrev(){
		musicSrv.playPrev();
        //dialog = ProgressDialog.show(MainActivity.this,"Loading","");
        dialog = ProgressDialog.show(MainActivity.this,"","Loading...");
        updateProgressBar();
		if(playbackPaused){
			setController();
			playbackPaused=false;
		}
		//controller.show(0);
	}

	@Override
	protected void onPause(){
		super.onPause();
		paused=true;
	}

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
	protected void onResume(){
		super.onResume();
		if(paused){
			setController();
			paused=false;
		}
	}

	@Override
	protected void onStop() {
		controller.hide();
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		stopService(playIntent);
		musicSrv=null;
		super.onDestroy();
	}


    class getMp3Link extends AsyncTask<Void, Objects, ArrayList<Song>> {
        ArrayList<Song> songlist;

        @Override
        protected ArrayList<Song> doInBackground(Void... arg0) {
            // TODO Auto-generated method stub
            RssReader reader = new RssReader(URL);


            try {
                //songlist = new ArrayList<>();
                songList = reader.getItems();
                //Log.e("TAG", items.toString());


            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            }


            return songList;

        }

        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show(MainActivity.this,"","Loading...");
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(ArrayList<Song> result) {

            super.onPostExecute(result);

            if(dialog.isShowing())
                dialog.dismiss();
            //TedAdapter adapter = new TedAdapter(getActivity(),items,R.id.action_settings);
            //grid.setAdapter(adapter);





        }




    }

}
