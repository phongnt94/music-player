package com.example.musicplayer;

/*
 *
 */

public class Song {
	
	private String link;
	private String title;
	private String artist;


    public void setLink(String link) {
        this.link = link;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public Song()
    {
        link="No link";
        title="No titile";
        artist="No artist";

    }

    public Song(String songID, String songTitle, String songArtist){
		link=songID;
		title=songTitle;
		artist=songArtist;
	}
	
	public String getID(){return link;}
	public String getTitle(){return title;}
	public String getArtist(){return artist;}

}

